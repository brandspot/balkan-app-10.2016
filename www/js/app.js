// Ionic Research Balkan Star App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var researchApp = angular.module('starter', ['ionic', 'ngCordova', 'ui.router','starter.controllers']);
var researchAppDB;

researchApp.run(function($ionicPlatform, $cordovaFile, $state, $cordovaSQLite) {
    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
            StatusBar.hide();
        }


        // $cordovaFile.removeFile(cordova.file.dataDirectory, "forms.txt")
        //       .then(function (success) {
        //         console.log('Config Remove');
        //       }, function (error) {
        //         // error
        //       });


        // $cordovaFile.createFile(cordova.file.dataDirectory, "forms.txt", true)
        //     .then(function (success) {
        //       console.log('Forms Create');
        //
        //     }, function (error) {
        //       console.log(error);
        //     });

        if (window.cordova) {
            researchAppDB = $cordovaSQLite.openDB({name: "research.db", location: 'default'});
        } else {
            researchAppDB = window.openDatabase("research.db", "1.0", "Web demo", 200000);
        }
        //$cordovaSQLite.execute(researchAppDB, "DROP TABLE IF EXISTS forms");
        $cordovaSQLite.execute(researchAppDB, "CREATE TABLE IF NOT EXISTS forms (id integer primary key, user_created text, data text, status text , rExCode text)");


        if(window.localStorage.getItem('rInCity') == null || window.localStorage.getItem('rInUserId') == null ||
            window.localStorage.getItem('rInTT') == null ){
            $state.go('authPage');
        }

        window.addEventListener('native.keyboardhide', function(){
            console.log('App keyboard-hide');
            $('form.auth-form').css({'top': '+=250'});
            $('form.interviewer-form').css({'top': '+=300'});
        });

        window.addEventListener('native.keyboardshow', function(){
            console.log('App keyboard-open');
            $('form.auth-form').css({'top': '-250px'});
            $('form.interviewer-form').css({'top': '-300px'});

        });

    });
    $ionicPlatform.registerBackButtonAction(function () {
        return false;
    }, 100);
});


researchApp.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('authPage', {
            url: '/auth-page',
            templateUrl: 'templates/auth.html',
            controller: 'authInterviewerCtrl'
        })


        .state('startWorkPage', {
            url: '/start-work-page',
            templateUrl: 'templates/start-work.html',
            controller: 'startWorkPageCtrl'
        })


        .state('signPage', {
            url: '/sign-page',
            templateUrl: 'templates/sign.html',
            controller: 'signPageCtrl'
        })


        .state('regInterviewerPage', {
            url: '/reg-interviewer-page',
            templateUrl: 'templates/reg-interviewer.html',
            controller: 'regInterviewerPageCtrl'
        })


        .state('oneQPage', {
            url: '/one-q-page',
            templateUrl: 'templates/one-q.html',
            controller: 'oneQPageCtrl'
        })

        .state('twoQPage', {
            url: '/two-q-page',
            templateUrl: 'templates/two-q.html',
            controller: 'twoQPageCtrl'
        })

        .state('threeQPage', {
            url: '/three-q-page',
            templateUrl: 'templates/three-q.html',
            controller: 'threeQPageCtrl'
        })

        .state('fourQPage', {
            url: '/four-q-page',
            templateUrl: 'templates/four-q.html',
            controller: 'fourQPageCtrl'
        })

        .state('fiveQPage', {
            url: '/five-q-page',
            templateUrl: 'templates/five-q.html',
            controller: 'fiveQPageCtrl'
        })

        .state('sixQPage', {
            url: '/six-q-page',
            templateUrl: 'templates/six-q.html',
            controller: 'sixQPageCtrl'
        })

        .state('lastPage', {
            url: '/last-page',
            templateUrl: 'templates/last.html',
            controller: 'lastPageCtrl'
        });



    $urlRouterProvider.otherwise('/start-work-page');
});

researchApp.config(function($ionicConfigProvider) {
    $ionicConfigProvider.views.transition('android');
});