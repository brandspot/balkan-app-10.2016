var controllerModule = angular.module('starter.controllers', []);


controllerModule.controller('authInterviewerCtrl', function($scope,$state,$cordovaFile,$ionicLoading) {

    $scope.user = {};
    $scope.user.login = 'Введите логин';
    $scope.user.adress = 'Введите адрес ТТ';

    var now = new Date();
    $scope.dateNow = now.getDate()+'.'+(now.getMonth()+1)+'.'+now.getFullYear()+' '+now.getHours()+':'+now.getMinutes();

    $scope.authUserGo = function (login, city, adress) {

        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Авторизация'
        });
        if(login == undefined || city == undefined || adress == undefined){
            //alert('Заполните все поля');
            $ionicLoading.hide();
            navigator.notification.alert(
                'Необходимо корректно заполнить все поля.',
                function(){},
                'Авторизация',
                'Закрыть'
            );
        }else{
            var id = checkInterviewer(login.toLowerCase());
            if(id > 0) {
                $ionicLoading.hide();
                $scope.nextPage(id, city, login, adress, now.getTime());
            }else{
                $ionicLoading.hide();
                navigator.notification.alert(
                    'Некоректный логин, доступ закрыт.',
                    function(){},
                    'Авторизация',
                    'Закрыть'
                );
            }
        }
    }


    $scope.nextPage = function(loginId, city, loginName, adress, date){

        window.localStorage.setItem('rInCity', city);
        window.localStorage.setItem('rInUserId', loginId);
        window.localStorage.setItem('rInUserLogin', loginName);
        window.localStorage.setItem('rInTT', adress);
        window.localStorage.setItem('rInDate', date);
        window.localStorage.setItem('rApp', 1);

        $state.go('startWorkPage');
    };
});


controllerModule.controller('startWorkPageCtrl', function($scope,$state,$cordovaFile,$ionicLoading, $ionicPlatform,$http,$cordovaNetwork,$cordovaSQLite) {

    $scope.userId = window.localStorage.getItem('rInUserId');

    if(window.localStorage.getItem('rInCity') == null || window.localStorage.getItem('rInUserId') == null ||
        window.localStorage.getItem('rInTT') == null ){
        $state.go('authPage');
    }

    $scope.userId = window.localStorage.getItem('rInUserId');

    $scope.createForm = function(){

        var saveForm = window.localStorage.getItem('saveForm');
        var rExCode = window.localStorage.getItem('rExCode');

        if(saveForm == 'NO' && rExCode != ''){

            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение последней анкеты на устройство!'
            });

            dataSave = {};

            dataSave.rExCode = window.localStorage.getItem('rExCode');
            dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
            dataSave.rInCity = window.localStorage.getItem('rInCity');
            dataSave.rInUserId = window.localStorage.getItem('rInUserId');
            dataSave.rInTT = window.localStorage.getItem('rInTT');
            dataSave.rInDate = window.localStorage.getItem('rInDate');
            dataSave.rApp = window.localStorage.getItem('rApp');
            dataSave.rUserLat = window.localStorage.getItem('rUserLat');
            dataSave.rUserLong = window.localStorage.getItem('rUserLong');
            dataSave.rUserSign = window.localStorage.getItem('rUserSign');
            dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
            dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
            dataSave.rUserTypeSig = window.localStorage.getItem('rUserTypeSig');
            dataSave.rUserName = window.localStorage.getItem('rUserName');
            dataSave.rUserName = window.localStorage.getItem('rUseSex');
            dataSave.rUserDate = window.localStorage.getItem('rUserDate');
            dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
            dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
            dataSave.rUserQ1 = window.localStorage.getItem('rUserQ1') != '' ? window.localStorage.getItem('rUserQ1') : '-';
            dataSave.rUserQ2 = window.localStorage.getItem('rUserQ2') != '' ? window.localStorage.getItem('rUserQ2') : '-';
            dataSave.rUserQ3 = window.localStorage.getItem('rUserQ3') != '' ? window.localStorage.getItem('rUserQ3') : '-';
            dataSave.rUserQ4 = window.localStorage.getItem('rUserQ4') != '' ? window.localStorage.getItem('rUserQ4') : '-';
            dataSave.rUserQ5 = window.localStorage.getItem('rUserQ5') != '' ? window.localStorage.getItem('rUserQ5') : '-';
            dataSave.rUserQ6 = window.localStorage.getItem('rUserQ6') != '' ? window.localStorage.getItem('rUserQ6') : '-';
            dataSave.rUserQ7 = window.localStorage.getItem('rUserQ7') != '' ? window.localStorage.getItem('rUserQ7') : '-';
            dataSave.rUserQ8 = window.localStorage.getItem('rUserQ8') != '' ? window.localStorage.getItem('rUserQ8') : '-';
            dataSave.rUserQ9 = window.localStorage.getItem('rUserQ9') != '' ? window.localStorage.getItem('rUserQ9') : '-';
            dataSave.rCurtForm = window.localStorage.getItem('rCurtForm');
            dataSave.rUserUsedSuperCode = window.localStorage.getItem('rUserUsedSuperCode');
            dataSave.rUserWinner = window.localStorage.getItem('rUserWinner');
            dataSave.rDateEnd = window.localStorage.getItem('rDateEnd');

            var strSave = JSON.stringify(dataSave);

            var query = "INSERT INTO forms (user_created, data, status, rExCode) VALUES (?,?,?,?)";
            $cordovaSQLite.execute(researchAppDB, query, [dataSave.rInUserId, strSave, 'NO', dataSave.rExCode]).then(function(res) {

                window.localStorage.setItem('saveForm', 'YES');
                document.location.href = 'index.html';
                $ionicLoading.hide();

            }, function (err) {

                document.location.href = 'index.html';
                $ionicLoading.hide();

            });

        } else {

            window.localStorage.removeItem('rExCode');
            window.localStorage.removeItem('rDateCreate');
            window.localStorage.removeItem('rCheckCode');
            window.localStorage.removeItem('rUserLat');
            window.localStorage.removeItem('rUserLong');
            window.localStorage.removeItem('rUserMarkaSig');
            window.localStorage.removeItem('rUserTypeSig');
            window.localStorage.removeItem('rUserName');
            window.localStorage.removeItem('rUserSex');
            window.localStorage.removeItem('rUserDate');
            window.localStorage.removeItem('rUserEmail');
            window.localStorage.removeItem('rUserPhone');
            window.localStorage.removeItem('rUserSign');
            window.localStorage.removeItem('rUserSignDate');
            window.localStorage.removeItem('rUserQ1');
            window.localStorage.removeItem('rUserQ2');
            window.localStorage.removeItem('rUserQ3');
            window.localStorage.removeItem('rUserQ4');
            window.localStorage.removeItem('rUserQ5');
            window.localStorage.removeItem('rUserQ6');
            window.localStorage.removeItem('rUserQ7');
            window.localStorage.removeItem('rUserQ8');
            window.localStorage.removeItem('rUserQ9');
            window.localStorage.removeItem('saveForm');
            window.localStorage.removeItem('rCurtForm');
            window.localStorage.removeItem('rUserUsedSuperCode');
            window.localStorage.removeItem('rUserWinner');
            window.localStorage.removeItem('rDateEnd');


            var dateForm = new Date().getTime();
            var exCode = window.localStorage.getItem('rInUserId') + '_' + dateForm;

            window.localStorage.setItem('rExCode', exCode);
            window.localStorage.setItem('rDateCreate', dateForm);
            window.localStorage.setItem('saveForm', 'NO');
            window.localStorage.setItem('rCurtForm', 1);
            window.localStorage.setItem('rUserQ1','-');
            window.localStorage.setItem('rUserQ2','-');
            window.localStorage.setItem('rUserQ3','-');
            window.localStorage.setItem('rUserQ4','-');
            window.localStorage.setItem('rUserQ5','-');
            window.localStorage.setItem('rUserQ6','-');
            window.localStorage.setItem('rUserQ7','-');
            window.localStorage.setItem('rUserQ8','-');
            window.localStorage.setItem('rUserQ9','-');
            window.localStorage.setItem('rUserWinner','NO');
            window.localStorage.setItem('rUserUsedSuperCode','NO');

            $state.go('regInterviewerPage');

        }

    };

    $scope.uploadForm = function () {

        if($cordovaNetwork.getNetwork() != 'none' && $cordovaNetwork.isOnline()){
            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Отправка анкет на сервер!'
            });


            var forms = [];
            var strData = "";
            var query = "SELECT * FROM forms WHERE status = ?";
            $cordovaSQLite.execute(researchAppDB, query, ['NO']).then(function(res) {

                for(var i = 0; i < res.rows.length; i++){
                    forms.push(res.rows.item(i));
                }

                for(var i = 0; i < forms.length; i++){
                    strData += forms[i]['data'] + '||';
                }
                strData = strData.substring(0, strData.length - 2);


                if(forms.length > 0) {
                    //console.log(forms);
                    $http.post('http://balkanka.global-man.ru/local/action/api.php', {method: 'save', data: strData}, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).success(function (result) {

                        navigator.notification.alert(
                            'Данные на сервере сохранены!',
                            function(){},
                            'Отправка анкет',
                            'Закрыть'
                        );

                        //console.log(result);
                        if (result.length > 0) {
                            var strWhere = "";
                            for (var i = result.length - 1; i >= 0; i--) {
                                if(result[i]['success'] || result[i]['mess'] == 'Вы пытаетесь повторно сохранить анкету.') {
                                    strWhere +=  "'"+result[i]['exCode']+"',";
                                }
                            }

                            if (strWhere.length > 5) {

                                strWhere = strWhere.substring(0, strWhere.length - 1);
                                var query = "UPDATE forms SET status = 'YES' WHERE rExCode IN ("+strWhere+")";
                                $cordovaSQLite.execute(researchAppDB, query).then(function(res) {
                                    //console.log(strWhere);
                                    $ionicLoading.hide();

                                }, function (err) {
                                    alert(err);
                                    //console.error(err);
                                    $ionicLoading.hide();

                                });
                            }

                        } else {

                            $ionicLoading.hide();
                        }
                        //alert('Данные на сервере сохранены!');

                    }).error(function(data, status, headers, config) {
                        $ionicLoading.hide();
                        if(status != ''){
                            navigator.notification.alert(
                                'Ошибка сохранения данных на сервере! Пожалуйста сделайте принтскрин этого сообщения и передайте его супервайзеру.\n\nКод ошибки: '+status+'.\n\nДополнительная информация: '+headers+'.',
                                function(){},
                                'Отправка анкет',
                                'Закрыть'
                            );
                        } else {
                            navigator.notification.alert(
                                'Ошибка сохранения данных на сервере! Пожалуйста сделайте принтскрин этого сообщения и передайте его супервайзеру.\n\nКод ошибки: '+status+'.\n\nДополнительная информация: '+headers+'.',
                                function(){},
                                'Отправка анкет',
                                'Закрыть'
                            );
                        }
                    });


                } else {
                    $ionicLoading.hide();
                    navigator.notification.alert(
                        'На устройстве нет анкет для отправки на сервер.',
                        function(){},
                        'Отправка анкет',
                        'Закрыть'
                    );
                    //alert('На устройстве нет анкет для отправки на сервер.');
                }


            }, function (err) {

                $ionicLoading.hide();
                alert(err);
                //console.error(err);
            });



        } else {
            navigator.notification.alert(
                'У вас отсутсвует интерент соединение!',
                function(){},
                'Отправка анкет',
                'Закрыть'
            );
        }
    }


    $scope.saveFiletoCashe = function () {
        var key = new Date().getTime();

        var forms = [];
        var strData = "";
        var query = "SELECT * FROM forms";
        $cordovaSQLite.execute(researchAppDB, query).then(function(res) {

            for(var i = 0; i < res.rows.length; i++){
                forms.push(res.rows.item(i));
            }

            for(var i = 0; i < forms.length; i++){
                strData += forms[i]['data'] + '||';
            }
            strData = strData.substring(0, strData.length - 2);


            if(forms.length > 0) {

                $cordovaFile.writeFile(cordova.file.externalDataDirectory, "form_"+key+".txt", strData, true)
                    .then(function (success) {
                        navigator.notification.alert(
                            'Файл успешно создан! Название файла forms'+key+'.txt.',
                            function(){},
                            'Сохранение анкет в файл',
                            'Закрыть'
                        );
                    }, function (error) {
                        navigator.notification.alert(
                            'Ошибка создания файла.',
                            function(){},
                            'Сохранение анкет в файл',
                            'Закрыть'
                        );
                    });


            } else {
                $ionicLoading.hide();
                navigator.notification.alert(
                    'На устройстве нет анкет для сохранения.',
                    function(){},
                    'Сохранение анкет в файл',
                    'Закрыть'
                );
                //alert('На устройстве нет анкет для отправки на сервер.');
            }


        }, function (err) {

            $ionicLoading.hide();
            alert(err);
            //console.error(err);
        });

    }


    $scope.exitApp = function () {

        window.localStorage.removeItem('rInCity');
        window.localStorage.removeItem('rInUserId');
        window.localStorage.removeItem('rInUserLogin');
        window.localStorage.removeItem('rInTT');
        window.localStorage.removeItem('rInDate');
        window.localStorage.removeItem('rApp');

        document.location.href = 'index.html';
    }


    $scope.showStat = function(){

        if($cordovaNetwork.getNetwork() != 'none' && $cordovaNetwork.isOnline()){
            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сбор статистики!'
            });

            var idLogin = window.localStorage.getItem('rInUserId');
            var nameLogin = window.localStorage.getItem('rInUserLogin');

            if(idLogin != '' && parseInt(idLogin) != 409){

                var forms = [];
                var query = "SELECT * FROM forms";
                $cordovaSQLite.execute(researchAppDB, query).then(function(res) {

                    for(var i = 0; i < res.rows.length; i++){
                        forms.push(res.rows.item(i));
                    }



                    if(forms.length > 0) {

                        var allFormsOnDevice = forms.length;
                        var messageStat = 'Вы авторизованы под логином '+nameLogin+'\n\nНА УСТРОЙСТВЕ\n\nВсего анкет: '+allFormsOnDevice+'\n';
                        var countFormsUserAll = 0;
                        var countFormsUserOne = 0;
                        var countFormsUserCurt = 0;
                        var countFormsUserAllServer = 0;
                        var countFormsUserOneServer = 0;

                        for (var i = forms.length - 1; i >= 0; i--) {
                            var objForm = JSON.parse(forms[i]['data']);

                            if(objForm.rInUserId == idLogin){
                                if(parseInt(objForm.rCurtForm) == 0){
                                    countFormsUserOne +=1;
                                }else{
                                    countFormsUserCurt +=1;
                                }
                                countFormsUserAll += 1;
                            }
                        };

                        messageStat += 'Всего анкет пользователя: '+countFormsUserAll+'\n';
                        messageStat += 'Завершенных анкет пользователя: '+countFormsUserOne+'\n';
                        messageStat += 'Прерваных анкет пользователя: '+countFormsUserCurt+'\n';

                        $http.post('http://balkanka.global-man.ru/local/action/api.php', {method: 'statistics', id: idLogin}, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function (result) {
                            $ionicLoading.hide();

                            if (result.success) {
                                countFormsUserAllServer = result.countFormsUserAllServer;
                                countFormsUserOneServer = result.countFormsUserOneServer;
                                //alert('Данные на сервере сохранены!');
                            } else {

                                //alert('Ошибка сохранения данных на сервере!');
                            }

                            messageStat += '\n\n\nНА СЕРВЕРЕ\n\n';
                            messageStat += 'Всего анкет пользователя: '+countFormsUserAllServer+'\n';
                            messageStat += 'Завершенных анкет пользователя: '+countFormsUserOneServer+'\n';

                            navigator.notification.alert(
                                messageStat,
                                function(){},
                                'Персональная статистика',
                                'Закрыть'
                            );

                            //alert(messageStat);

                        }).error(function(data, status, headers, config) {
                            $ionicLoading.hide();
                            if(status != ''){
                                navigator.notification.alert(
                                    'Ошибка получения данных! Пожалуйста сделайте принтскрин этого сообщения и передайте его супервайзеру.\n\nКод ошибки: '+status+'.\n\nДополнительная информация: '+headers+'.',
                                    function(){},
                                    'Персональная статистика',
                                    'Закрыть'
                                );
                            } else {
                                navigator.notification.alert(
                                    'Ошибка получения данных! Пожалуйста сделайте принтскрин этого сообщения и передайте его супервайзеру.\n\nКод ошибки: '+status+'.\n\nДополнительная информация: '+headers+'.',
                                    function(){},
                                    'Персональная статистика',
                                    'Закрыть'
                                );
                            }
                        });

                    } else {
                        $ionicLoading.hide();
                        navigator.notification.alert(
                            'На устройстве нет анкет.',
                            function(){},
                            'Персональная статистика',
                            'Закрыть'
                        );
                    }


                }, function (err) {
                    //console.error(err);
                });

                $ionicLoading.hide();

            } else {
                $ionicLoading.hide();
                navigator.notification.alert(
                    'В этом режиме вы не можете просматривать статистику!',
                    function(){},
                    'Персональная статистика',
                    'Закрыть'
                );
            }

        } else {
            navigator.notification.alert(
                'У вас отсутсвует интерент соединение!',
                function(){},
                'Персональная статистика',
                'Закрыть'
            );
        }
    }

});


controllerModule.controller('regInterviewerPageCtrl', function($scope,$state,$cordovaGeolocation,$http,$ionicLoading,$cordovaNetwork) {

    $scope.form = {};
    $scope.form.UserName = 'Введите Имя и Фамилию';
    $scope.form.rUserPhone = 'Введите телефон (79001112233)';
    $scope.form.UserEmail = 'Введите email';

    var userDateForm;

    var todayMinBorn = new Date();
    todayMinBorn.setDate(todayMinBorn.getDate()-1);
    todayMinBorn.setYear(todayMinBorn.getFullYear()-18);

    var todayMinBornMonth = todayMinBorn.getMonth()+1;
    var todayMinBornDate = todayMinBorn.getDate();
    if (todayMinBornMonth < 10) todayMinBornMonth = '0' + todayMinBornMonth;
    if (todayMinBornDate <10) todayMinBornDate = '0' + todayMinBornDate;

    $('.interviewer-page-i4 input').attr('max',todayMinBorn.getFullYear()+'-'+todayMinBornMonth+'-'+todayMinBornDate);
    $('.interviewer-page-i4 input').attr('value',todayMinBorn.getFullYear()+'-'+todayMinBornMonth+'-'+todayMinBornDate);

    if($cordovaNetwork.getNetwork() == 'none' && !$cordovaNetwork.isOnline()){
        navigator.notification.alert(
            'Внимание, у вас отсутсвует интерент соединение! Вы не сможете проверить подлиность электронной почты или телефонного номера.',
            function(){},
            'Регистрационные данные',
            'Закрыть'
        );
    }

    $scope.nextPage = function(formMarkaSig, formTypeSig, formUserName, formUserSex, formUserDate, formUserEmail, formrUserPhone, formCode){

        if(!!formUserDate) {
            var userDate = new Date(formUserDate);
            var userDateTimestamp = userDate.getTime();
            userDateForm = (userDate.getDate()+1) + '.' + (userDate.getMonth() + 1) + '.' + userDate.getFullYear();
        }


        if (formUserName == 'Введите Имя и Фамилию') {
            formUserName = '';
        }

        if (formUserEmail == 'Введите email') {
            formUserEmail = '';
        }

        if (formrUserPhone == 'Введите телефон (79001112233)') {
            formrUserPhone = '';
        }

        if(!!formMarkaSig && !!formTypeSig && !!formUserName && !!formUserDate && !!formrUserPhone && !!formUserSex) {

            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Проверка введёных данных.'
            });

            var errArr = [];

            if (!!formUserEmail) {
                if (checkEmail(formUserEmail) === false) {
                    errArr.push('-Введите корректный email.');
                }
            }

            if (checkTextCiril(formUserName) === false) {
                errArr.push('-Введите корректное имя и фамилию.');
            }

            if (checkNameAndLastName(formUserName) === false) {
                errArr.push('-Необходимо ввести Имя и Фамилию.');
            }

            if (parseInt(formrUserPhone.substr(0,1)) != 7) {
                errArr.push('-Номер телефона должен начинаться с 7.');
            }

            if (formrUserPhone.length != 11) {
                errArr.push('-Номер телефона должен состоять из 11 символов.');
            }

//            if (parseInt(getAge(userDateForm)) <= 18) {
//                errArr.push('-Вам должно быть больше 18 лет.');
//            }


            if (errArr.length > 0){

                $ionicLoading.hide();

                var messageErr = '';
                for (var i = errArr.length - 1; i >= 0; i--) {
                    messageErr = messageErr +'\n\r'+ errArr[i];
                };

                navigator.notification.alert(
                    messageErr,
                    function(){},
                    'Регистрационные данные',
                    'Исправить'
                );
                //alert(messageErr);

            } else{


                var posOptions = {timeout: 10000, enableHighAccuracy: false};
                $cordovaGeolocation
                    .getCurrentPosition(posOptions)
                    .then(function (position) {
                        window.localStorage.setItem('rUserLat', position.coords.latitude);
                        window.localStorage.setItem('rUserLong', position.coords.longitude);
                    }, function(err) {
                        // error
                    });

                window.localStorage.setItem('rUserMarkaSig', formMarkaSig);
                window.localStorage.setItem('rUserTypeSig', formTypeSig);
                window.localStorage.setItem('rUserName', formUserName);
                window.localStorage.setItem('rUserSex', formUserSex);
                window.localStorage.setItem('rUserDate', userDateTimestamp);
                window.localStorage.setItem('rUserEmail', formUserEmail);
                window.localStorage.setItem('rUserPhone', formrUserPhone);

                var today = new Date();
                var superCode = today.getFullYear() - today.getDate() + today.getHours();

                if (!!formCode && !!window.localStorage.getItem('rCheckCode')) {
                    $ionicLoading.hide();
                    if (formCode == window.localStorage.getItem('rCheckCode')  || formCode == superCode || formCode == '0001') {
                        if (formCode == superCode) {
                            window.localStorage.setItem('rUserUsedSuperCode', 'YES');
                        }
                        $state.go('signPage');
                    } else {
                        navigator.notification.alert(
                            'Введён неверный код. Попробуйте отправить код ещё раз.',
                            function(){},
                            'Проверка кода',
                            'Закрыть'
                        );
                        //alert('Введён неверный код. Попробуйте отправить код ещё раз.');
                    }
                } else {
                    if (!!formUserEmail) {
                        $http.post('http://balkanka.global-man.ru/local/action/api.php', {
                            method: 'emailValidation',
                            email: formUserEmail
                        }, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function (result) {

                            if (result.success) {
                                navigator.notification.alert(
                                    'Электронная почта прошла проверку!',
                                    function () {
                                    },
                                    'Регистрационные данные',
                                    'Закрыть'
                                );
                                $state.go('signPage');
                                //alert('Прошел проверку');
                            } else {
                                navigator.notification.alert(
                                    'Электронная почта не прошла проверку!',
                                    function () {
                                    },
                                    'Регистрационные данные',
                                    'Закрыть'
                                );
                                //alert('Не прошел проверку');
                                $('.interviewer-page-i7').show();
                                $('.interviewer-page-i8').show();
                            }
                            $ionicLoading.hide();
                        }).error(function (data, status, headers, config) {
                            navigator.notification.alert(
                                'В настоящий момент электронную почту не возможно проверить. Повторите попытку позже или воспользуйтесь проверкой телефонного номера.',
                                function () {
                                },
                                'Регистрационные данные',
                                'Закрыть'
                            );
                            $('.interviewer-page-i7').show();
                            $('.interviewer-page-i8').show();
                            $ionicLoading.hide();
                        });
                    } else {
                        navigator.notification.alert(
                            'Вам необходимо подтвердить телефонный номер.',
                            function () {
                            },
                            'Регистрационные данные',
                            'Закрыть'
                        );
                        $('.interviewer-page-i7').show();
                        $('.interviewer-page-i8').show();
                        $ionicLoading.hide();
                    }
                }
            }
        }else{
            $ionicLoading.hide();
            navigator.notification.alert(
                'Необходимо корректно заполнить все поля.',
                function(){},
                'Регистрационные данные',
                'Закрыть'
            );
            //alert('Необходимо корректно заполнить все поля.');
        }
    };


    $scope.getCode = function (formrUserPhone) {

        var code = genereateCode();

        $http.post('http://balkanka.global-man.ru/local/action/api.php', {method: 'getSmsCode', code: code, phone: formrUserPhone}, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).success(function (result) {
            if (result.success) {
                window.localStorage.setItem('rCheckCode', code);
                navigator.notification.alert(
                    'Код отправлен на указынный номер, пожалуйста введите код и нажмите кнопку вперёд.',
                    function(){},
                    'Отправка кода',
                    'Закрыть'
                );
                //alert('Код '+code+' отправлен на указынный номер, пожалуйста введите код и нажмите кнопку вперёд.');
            } else {
                navigator.notification.alert(
                    'Код не был отправлен, пожалуйста, повторите попытку.',
                    function(){},
                    'Отправка кода',
                    'Закрыть'
                );
                //alert('Код не был отправлен, пожалуйста, повторите попытку.');
            }
        }).error(function(data, status, headers, config) {
            window.localStorage.setItem('rCheckCode', code);
            navigator.notification.alert(
                'В настоящий момент не возможно отправить код для проверки телефонного номера. Повторите попытку позже или запросите у супервайзера персональный временный код.',
                function(){},
                'Регистрационные данные',
                'Закрыть'
            );

        });
    }

});


controllerModule.controller('signPageCtrl', function($scope,$state) {

    var now = new Date();
    $scope.dateNow = now.getDate()+'.'+(now.getMonth()+1)+'.'+now.getFullYear()+' '+now.getHours()+':'+now.getMinutes();
    $scope.userName = window.localStorage.getItem('rUserName');

    var CanvasDrawr = function(options) {
        var canvas = document.getElementById(options.id),
            ctxt = canvas.getContext("2d");
        ctxt.clearRect(0, 0, canvas.width, canvas.height);
        ctxt.lineWidth = options.size;
        ctxt.lineCap = "round";
        ctxt.pX = undefined;
        ctxt.pY = undefined;
        var lines = [,,];
        var self = {
            init: function() {

                canvas.addEventListener('touchstart', self.preDraw, false);
                canvas.addEventListener('touchmove', self.draw, false);
                canvas.addEventListener('touchend', self.save, false);
            },
            preDraw: function(event) {
                $.each(event.touches, function(i, touch) {
                    var id = touch.identifier;
                    lines[id] = { x     : this.pageX - 370,
                        y     : this.pageY - 610,
                        color : options.color
                    };
                });
                event.preventDefault();
            },
            draw: function(event) {
                var e = event, hmm = {};
                $.each(event.touches, function(i, touch) {
                    var id = touch.identifier;
                    var moveX = this.pageX - 370 - lines[id].x,
                        moveY = this.pageY - 610 - lines[id].y;
                    var ret = self.move(id, moveX, moveY);
                    lines[id].x = ret.x;
                    lines[id].y = ret.y;
                });
                event.preventDefault();
            },
            move: function(i, changeX, changeY) {
                ctxt.strokeStyle = lines[i].color;
                ctxt.beginPath();
                ctxt.moveTo(lines[i].x, lines[i].y);
                ctxt.lineTo(lines[i].x + changeX, lines[i].y + changeY);
                ctxt.stroke();
                ctxt.closePath();
                return { x: lines[i].x + changeX, y: lines[i].y + changeY };
            },
            save: function () {
                var pngUrl = canvas.toDataURL();
                window.localStorage.setItem('rUserSign', pngUrl);

            }
        };
        return self.init();
    };
    var canvas_drawing = new CanvasDrawr({id:"canvas", size: 2, color: "red" });

    $scope.clearSign = function(){
        var canvas = document.getElementById("canvas"),
            ctxt = canvas.getContext("2d");
        ctxt.clearRect(0, 0, canvas.width, canvas.height);
    };

    $scope.nextPage = function(agree){

        if (!!window.localStorage.getItem('rUserSign') && agree === true) {
            var dateNow = new Date().getTime();

            window.localStorage.setItem('rUserSignDate', dateNow);

            if (window.localStorage.getItem('rUserMarkaSig') == 'Балканская Звезда') {
                $state.go('oneQPage');
            } else {
                $state.go('fourQPage');
            }
        } else {

            navigator.notification.alert(
                'Вам необходимо оставить электронную подпись.',
                function(){},
                'Электронная подпись',
                'Исправить'
            );

        }
    };

});


controllerModule.controller('oneQPageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading,$cordovaSQLite) {

    $scope.nextPage = function(formUserQ1){
        window.localStorage.setItem('rUserQ1', formUserQ1);
        $state.go('twoQPage');
    };


    $scope.stopPage = function(){

        var dateFormEnd = new Date().getTime();
        window.localStorage.setItem('rDateEnd', dateFormEnd);

        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты на устройство!'
        });
        var dataSave = {};

        dataSave.rExCode = window.localStorage.getItem('rExCode');
        dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
        dataSave.rInCity = window.localStorage.getItem('rInCity');
        dataSave.rInUserId = window.localStorage.getItem('rInUserId');
        dataSave.rInTT = window.localStorage.getItem('rInTT');
        dataSave.rInDate = window.localStorage.getItem('rInDate');
        dataSave.rApp = window.localStorage.getItem('rApp');
        dataSave.rUserLat = window.localStorage.getItem('rUserLat');
        dataSave.rUserLong = window.localStorage.getItem('rUserLong');
        dataSave.rUserSign = window.localStorage.getItem('rUserSign');
        dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
        dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
        dataSave.rUserTypeSig = window.localStorage.getItem('rUserTypeSig');
        dataSave.rUserName = window.localStorage.getItem('rUserName');
        dataSave.rUserSex = window.localStorage.getItem('rUserSex');
        dataSave.rUserDate = window.localStorage.getItem('rUserDate');
        dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
        dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
        dataSave.rUserQ1 = '-';
        dataSave.rUserQ2 = '-';
        dataSave.rUserQ3 = '-';
        dataSave.rUserQ4 = '-';
        dataSave.rUserQ5 = '-';
        dataSave.rUserQ6 = '-';
        dataSave.rUserQ7 = '-';
        dataSave.rUserQ8 = '-';
        dataSave.rUserQ9 = '-';
        dataSave.rCurtForm = 1;
        dataSave.rUserUsedSuperCode = window.localStorage.getItem('rUserUsedSuperCode');
        dataSave.rUserWinner = 'NO';
        dataSave.rDateEnd = dateFormEnd;

        var strSave = JSON.stringify(dataSave);

        var query = "INSERT INTO forms (user_created, data, status, rExCode) VALUES (?,?,?,?)";
        $cordovaSQLite.execute(researchAppDB, query, [dataSave.rInUserId, strSave, 'NO', dataSave.rExCode]).then(function(res) {

            window.localStorage.setItem('saveForm', 'YES');
            document.location.href = 'index.html';
            $ionicLoading.hide();

        }, function (err) {

            document.location.href = 'index.html';
            $ionicLoading.hide();

        });
    };


});

controllerModule.controller('twoQPageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading,$cordovaSQLite) {

    $scope.userLastName =  window.localStorage.getItem('rUserLastName');
    $scope.userName = window.localStorage.getItem('rUserName');

    $scope.nextPage = function(formUserQ2){
        window.localStorage.setItem('rUserQ2', formUserQ2);
        $state.go('threeQPage');
    };


    $scope.stopPage = function(){

        var dateFormEnd = new Date().getTime();
        window.localStorage.setItem('rDateEnd', dateFormEnd);

        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты на устройство!'
        });
        var dataSave = {};

        dataSave.rExCode = window.localStorage.getItem('rExCode');
        dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
        dataSave.rInCity = window.localStorage.getItem('rInCity');
        dataSave.rInUserId = window.localStorage.getItem('rInUserId');
        dataSave.rInTT = window.localStorage.getItem('rInTT');
        dataSave.rInDate = window.localStorage.getItem('rInDate');
        dataSave.rApp = window.localStorage.getItem('rApp');
        dataSave.rUserLat = window.localStorage.getItem('rUserLat');
        dataSave.rUserLong = window.localStorage.getItem('rUserLong');
        dataSave.rUserSign = window.localStorage.getItem('rUserSign');
        dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
        dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
        dataSave.rUserTypeSig = window.localStorage.getItem('rUserTypeSig');
        dataSave.rUserName = window.localStorage.getItem('rUserName');
        dataSave.rUserSex = window.localStorage.getItem('rUserSex');
        dataSave.rUserDate = window.localStorage.getItem('rUserDate');
        dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
        dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
        dataSave.rUserQ1 = window.localStorage.getItem('rUserQ1');
        dataSave.rUserQ2 = '-';
        dataSave.rUserQ3 = '-';
        dataSave.rUserQ4 = '-';
        dataSave.rUserQ5 = '-';
        dataSave.rUserQ6 = '-';
        dataSave.rCurtForm = 1;
        dataSave.rUserUsedSuperCode = window.localStorage.getItem('rUserUsedSuperCode');
        dataSave.rUserWinner = 'NO';
        dataSave.rDateEnd = dateFormEnd;

        var strSave = JSON.stringify(dataSave);

        var query = "INSERT INTO forms (user_created, data, status, rExCode) VALUES (?,?,?,?)";
        $cordovaSQLite.execute(researchAppDB, query, [dataSave.rInUserId, strSave, 'NO', dataSave.rExCode]).then(function(res) {

            window.localStorage.setItem('saveForm', 'YES');
            document.location.href = 'index.html';
            $ionicLoading.hide();

        }, function (err) {

            document.location.href = 'index.html';
            $ionicLoading.hide();

        });

    };

});

controllerModule.controller('threeQPageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading,$cordovaSQLite) {

    $scope.userQ1 = window.localStorage.getItem('rUserQ1');

    $scope.nextPage = function(formUserQ3){
        window.localStorage.setItem('rUserQ3', formUserQ3);
        $state.go('lastPage');
    };

    $scope.stopPage = function(){

        var dateFormEnd = new Date().getTime();
        window.localStorage.setItem('rDateEnd', dateFormEnd);

        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты на устройство!'
        });
        var dataSave = {};

        dataSave.rExCode = window.localStorage.getItem('rExCode');
        dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
        dataSave.rInCity = window.localStorage.getItem('rInCity');
        dataSave.rInUserId = window.localStorage.getItem('rInUserId');
        dataSave.rInTT = window.localStorage.getItem('rInTT');
        dataSave.rInDate = window.localStorage.getItem('rInDate');
        dataSave.rApp = window.localStorage.getItem('rApp');
        dataSave.rUserLat = window.localStorage.getItem('rUserLat');
        dataSave.rUserLong = window.localStorage.getItem('rUserLong');
        dataSave.rUserSign = window.localStorage.getItem('rUserSign');
        dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
        dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
        dataSave.rUserTypeSig = window.localStorage.getItem('rUserTypeSig');
        dataSave.rUserName = window.localStorage.getItem('rUserName');
        dataSave.rUserSex = window.localStorage.getItem('rUserSex');
        dataSave.rUserDate = window.localStorage.getItem('rUserDate');
        dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
        dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
        dataSave.rUserQ1 = window.localStorage.getItem('rUserQ1');
        dataSave.rUserQ2 = window.localStorage.getItem('rUserQ2');
        dataSave.rUserQ3 = '-';
        dataSave.rUserQ4 = '-';
        dataSave.rUserQ5 = '-';
        dataSave.rUserQ6 = '-';
        dataSave.rCurtForm = 1;
        dataSave.rUserUsedSuperCode = window.localStorage.getItem('rUserUsedSuperCode');
        dataSave.rUserWinner = 'NO';
        dataSave.rDateEnd = dateFormEnd;

        var strSave = JSON.stringify(dataSave);

        var query = "INSERT INTO forms (user_created, data, status, rExCode) VALUES (?,?,?,?)";
        $cordovaSQLite.execute(researchAppDB, query, [dataSave.rInUserId, strSave, 'NO', dataSave.rExCode]).then(function(res) {

            window.localStorage.setItem('saveForm', 'YES');
            document.location.href = 'index.html';
            $ionicLoading.hide();

        }, function (err) {

            document.location.href = 'index.html';
            $ionicLoading.hide();

        });

    };

});

controllerModule.controller('fourQPageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading,$cordovaSQLite) {


    $scope.nextPage = function(formUserQ4){
        window.localStorage.setItem('rUserQ4', formUserQ4);
        $state.go('fiveQPage');
    };


    $scope.stopPage = function(){

        var dateFormEnd = new Date().getTime();
        window.localStorage.setItem('rDateEnd', dateFormEnd);

        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты на устройство!'
        });
        var dataSave = {};

        dataSave.rExCode = window.localStorage.getItem('rExCode');
        dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
        dataSave.rInCity = window.localStorage.getItem('rInCity');
        dataSave.rInUserId = window.localStorage.getItem('rInUserId');
        dataSave.rInTT = window.localStorage.getItem('rInTT');
        dataSave.rInDate = window.localStorage.getItem('rInDate');
        dataSave.rApp = window.localStorage.getItem('rApp');
        dataSave.rUserLat = window.localStorage.getItem('rUserLat');
        dataSave.rUserLong = window.localStorage.getItem('rUserLong');
        dataSave.rUserSign = window.localStorage.getItem('rUserSign');
        dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
        dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
        dataSave.rUserTypeSig = window.localStorage.getItem('rUserTypeSig');
        dataSave.rUserName = window.localStorage.getItem('rUserName');
        dataSave.rUserSex = window.localStorage.getItem('rUserSex');
        dataSave.rUserDate = window.localStorage.getItem('rUserDate');
        dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
        dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
        dataSave.rUserQ1 = '-';
        dataSave.rUserQ2 = '-';
        dataSave.rUserQ3 = '-';
        dataSave.rUserQ4 = '-';
        dataSave.rUserQ5 = '-';
        dataSave.rUserQ6 = '-';
        dataSave.rUserWinner = 'NO';
        dataSave.rCurtForm = 1;
        dataSave.rUserUsedSuperCode = window.localStorage.getItem('rUserUsedSuperCode');
        dataSave.rDateEnd = dateFormEnd;

        var strSave = JSON.stringify(dataSave);

        var query = "INSERT INTO forms (user_created, data, status, rExCode) VALUES (?,?,?,?)";
        $cordovaSQLite.execute(researchAppDB, query, [dataSave.rInUserId, strSave, 'NO', dataSave.rExCode]).then(function(res) {

            window.localStorage.setItem('saveForm', 'YES');
            document.location.href = 'index.html';
            $ionicLoading.hide();

        }, function (err) {

            document.location.href = 'index.html';
            $ionicLoading.hide();

        });
    };

});

controllerModule.controller('fiveQPageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading,$cordovaSQLite) {


    $scope.nextPage = function(formUserQ5){
        window.localStorage.setItem('rUserQ5', formUserQ5);
        $state.go('sixQPage');
    };


    $scope.stopPage = function(){

        var dateFormEnd = new Date().getTime();
        window.localStorage.setItem('rDateEnd', dateFormEnd);

        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты на устройство!'
        });
        var dataSave = {};

        dataSave.rExCode = window.localStorage.getItem('rExCode');
        dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
        dataSave.rInCity = window.localStorage.getItem('rInCity');
        dataSave.rInUserId = window.localStorage.getItem('rInUserId');
        dataSave.rInTT = window.localStorage.getItem('rInTT');
        dataSave.rInDate = window.localStorage.getItem('rInDate');
        dataSave.rApp = window.localStorage.getItem('rApp');
        dataSave.rUserLat = window.localStorage.getItem('rUserLat');
        dataSave.rUserLong = window.localStorage.getItem('rUserLong');
        dataSave.rUserSign = window.localStorage.getItem('rUserSign');
        dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
        dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
        dataSave.rUserTypeSig = window.localStorage.getItem('rUserTypeSig');
        dataSave.rUserName = window.localStorage.getItem('rUserName');
        dataSave.rUserSex = window.localStorage.getItem('rUserSex');
        dataSave.rUserDate = window.localStorage.getItem('rUserDate');
        dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
        dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
        dataSave.rUserQ1 = '-';
        dataSave.rUserQ2 = '-';
        dataSave.rUserQ3 = '-';
        dataSave.rUserQ4 = window.localStorage.getItem('rUserQ4');
        dataSave.rUserQ5 = '-';
        dataSave.rUserQ6 = '-';
        dataSave.rUserWinner = 'NO';
        dataSave.rCurtForm = 1;
        dataSave.rUserUsedSuperCode = window.localStorage.getItem('rUserUsedSuperCode');
        dataSave.rDateEnd = dateFormEnd;

        var strSave = JSON.stringify(dataSave);

        var query = "INSERT INTO forms (user_created, data, status, rExCode) VALUES (?,?,?,?)";
        $cordovaSQLite.execute(researchAppDB, query, [dataSave.rInUserId, strSave, 'NO', dataSave.rExCode]).then(function(res) {

            window.localStorage.setItem('saveForm', 'YES');
            document.location.href = 'index.html';
            $ionicLoading.hide();

        }, function (err) {

            document.location.href = 'index.html';
            $ionicLoading.hide();

        });
    };

});

controllerModule.controller('sixQPageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading,$cordovaSQLite) {


    $scope.nextPage = function(formUserQ6){
        window.localStorage.setItem('rUserQ6', formUserQ6);
        $state.go('lastPage');
    };


    $scope.stopPage = function(){

        var dateFormEnd = new Date().getTime();
        window.localStorage.setItem('rDateEnd', dateFormEnd);

        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты на устройство!'
        });
        var dataSave = {};

        dataSave.rExCode = window.localStorage.getItem('rExCode');
        dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
        dataSave.rInCity = window.localStorage.getItem('rInCity');
        dataSave.rInUserId = window.localStorage.getItem('rInUserId');
        dataSave.rInTT = window.localStorage.getItem('rInTT');
        dataSave.rInDate = window.localStorage.getItem('rInDate');
        dataSave.rApp = window.localStorage.getItem('rApp');
        dataSave.rUserLat = window.localStorage.getItem('rUserLat');
        dataSave.rUserLong = window.localStorage.getItem('rUserLong');
        dataSave.rUserSign = window.localStorage.getItem('rUserSign');
        dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
        dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
        dataSave.rUserTypeSig = window.localStorage.getItem('rUserTypeSig');
        dataSave.rUserName = window.localStorage.getItem('rUserName');
        dataSave.rUserSex = window.localStorage.getItem('rUserSex');
        dataSave.rUserDate = window.localStorage.getItem('rUserDate');
        dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
        dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
        dataSave.rUserQ1 = '-';
        dataSave.rUserQ2 = '-';
        dataSave.rUserQ3 = '-';
        dataSave.rUserQ4 = window.localStorage.getItem('rUserQ4');
        dataSave.rUserQ5 = window.localStorage.getItem('rUserQ5');
        dataSave.rUserQ6 = '-';
        dataSave.rUserWinner = 'NO';
        dataSave.rCurtForm = 1;
        dataSave.rUserUsedSuperCode = window.localStorage.getItem('rUserUsedSuperCode');
        dataSave.rDateEnd = dateFormEnd;

        var strSave = JSON.stringify(dataSave);

        var query = "INSERT INTO forms (user_created, data, status, rExCode) VALUES (?,?,?,?)";
        $cordovaSQLite.execute(researchAppDB, query, [dataSave.rInUserId, strSave, 'NO', dataSave.rExCode]).then(function(res) {

            window.localStorage.setItem('saveForm', 'YES');
            document.location.href = 'index.html';
            $ionicLoading.hide();

        }, function (err) {

            document.location.href = 'index.html';
            $ionicLoading.hide();

        });
    };

});



controllerModule.controller('lastPageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading,$cordovaSQLite) {

    var rUserWinner = 'Зажигалка';

    $scope.nextPage = function(){

        if(rUserWinner != '') {

            var dateFormEnd = new Date().getTime();
            window.localStorage.setItem('rDateEnd', dateFormEnd);
            window.localStorage.setItem('rUserWinner', rUserWinner);
            window.localStorage.setItem('rCurtForm', 0);

            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты!'
            });
            var dataSave = {};

            dataSave.rExCode = window.localStorage.getItem('rExCode');
            dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
            dataSave.rInCity = window.localStorage.getItem('rInCity');
            dataSave.rInUserId = window.localStorage.getItem('rInUserId');
            dataSave.rInTT = window.localStorage.getItem('rInTT');
            dataSave.rInDate = window.localStorage.getItem('rInDate');
            dataSave.rApp = window.localStorage.getItem('rApp');
            dataSave.rUserLat = window.localStorage.getItem('rUserLat');
            dataSave.rUserLong = window.localStorage.getItem('rUserLong');
            dataSave.rUserSign = window.localStorage.getItem('rUserSign');
            dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
            dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
            dataSave.rUserTypeSig = window.localStorage.getItem('rUserTypeSig');
            dataSave.rUserName = window.localStorage.getItem('rUserName');
            dataSave.rUserSex = window.localStorage.getItem('rUserSex');
            dataSave.rUserDate = window.localStorage.getItem('rUserDate');
            dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
            dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
            dataSave.rUserQ1 = window.localStorage.getItem('rUserQ1');
            dataSave.rUserQ2 = window.localStorage.getItem('rUserQ2');
            dataSave.rUserQ3 = window.localStorage.getItem('rUserQ3');
            dataSave.rUserQ4 = window.localStorage.getItem('rUserQ4');
            dataSave.rUserQ5 = window.localStorage.getItem('rUserQ5');
            dataSave.rUserQ6 = window.localStorage.getItem('rUserQ6');
            dataSave.rCurtForm = 0;
            dataSave.rUserWinner = rUserWinner;
            dataSave.rUserUsedSuperCode = window.localStorage.getItem('rUserUsedSuperCode');
            dataSave.rDateEnd = dateFormEnd;

            var strSave = JSON.stringify(dataSave);

            var query = "INSERT INTO forms (user_created, data, status, rExCode) VALUES (?,?,?,?)";
            $cordovaSQLite.execute(researchAppDB, query, [dataSave.rInUserId, strSave, 'NO', dataSave.rExCode]).then(function(res) {
                window.localStorage.setItem('saveForm', 'YES');
                $http.post('http://balkanka.global-man.ru/local/action/api.php', {method: 'save', data: strSave}, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    }
                }).success(function (result) {
                    if (result.length > 0) {
                        var strWhere = "";
                        for (var i = result.length - 1; i >= 0; i--) {
                            if(result[i]['success']) {
                                strWhere +=  "'"+result[i]['exCode']+"',";
                            }
                        }

                        if (strWhere.length > 10) {

                            strWhere = strWhere.substring(0, strWhere.length - 1);
                            var query = "UPDATE forms SET status = 'YES' WHERE rExCode IN ("+strWhere+")";
                            $cordovaSQLite.execute(researchAppDB, query).then(function(res) {

                                navigator.notification.alert(
                                    'Анкета успешно сохранена на устройстве и на сервере.',
                                    function(){},
                                    'Внимание',
                                    'Закрыть'
                                );
                                //alert('Анкета успешно сохранена на устройстве и на сервере.');
                                document.location.href = 'index.html';
                                $ionicLoading.hide();

                            }, function (err) {

                                document.location.href = 'index.html';
                                $ionicLoading.hide();

                            });
                        }

                    } else {
                        navigator.notification.alert(
                            'Анкета успешно сохранена на устройстве.',
                            function(){},
                            'Внимание',
                            'Закрыть'
                        );
                        //alert('Анкета успешно сохранена на устройстве.');
                        document.location.href = 'index.html';
                        $ionicLoading.hide();
                    }


                }).error(function(data, status, headers, config) {
                    //console.log(data);
                    //console.log(headers);
                    navigator.notification.alert(
                        'Анкета успешно сохранена на устройстве.',
                        function(){},
                        'Внимание',
                        'Закрыть'
                    );
                    document.location.href = 'index.html';
                    $ionicLoading.hide();
                });

            }, function (err) {

                document.location.href = 'index.html';
                $ionicLoading.hide();

            });


        }else {
            navigator.notification.alert(
                'Необходимо выбрать вид оплаты.',
                function(){},
                'Внимание',
                'Выбрать'
            );
            //alert('Необходимо выбрать один из двух видов оплаты.');
        }

    };

});


// More check functions

function checkEmail(email){
    var re = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,6}$/i;
    return re.test(email) ? true : false;
};

function checkTextCiril(string) {
    var re = /^[а-яё, .\s]+$/i;
    if (string == '' || !re.test(string)) {
        return false;
    } else {
        return true;
    }
}

function checkTextCirilWithNumber(string) {
    var re = /^[а-яё,.\s]+$/i;
    if (string == '' || !re.test(string)) {
        return false;
    } else {
        return true;
    }
}

function getAge(dateString) {
    var day = parseInt(dateString.substring(0,2));
    var month = parseInt(dateString.substring(3,5));
    var year = parseInt(dateString.substring(6,10));

    var today = new Date();
    var birthDate = new Date(year, month, day);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()+2)) {
        age--;
    }
    return age;
}

function checkInterviewer(login) {

    var dataInterviewers = [{"id":"1","login":"test101"},{"id":"2","login":"test102"},{"id":"3","login":"test103"},{"id":"4","login":"test104"},{"id":"5","login":"test105"},{"id":"6","login":"vologda2101"},{"id":"7","login":"vologda2102"},{"id":"8","login":"vologda2103"},{"id":"9","login":"vologda2104"},{"id":"10","login":"vologda2105"},{"id":"11","login":"vologda2106"},{"id":"12","login":"vologda2107"},{"id":"13","login":"vologda2108"},{"id":"14","login":"vologda2109"},{"id":"15","login":"vologda2110"},{"id":"16","login":"vologda2111"},{"id":"17","login":"vologda2112"},{"id":"18","login":"vologda2113"},{"id":"19","login":"vologda2114"},{"id":"20","login":"vologda2115"},{"id":"21","login":"vologda2116"},{"id":"22","login":"vologda2117"},{"id":"23","login":"vologda2118"},{"id":"24","login":"vologda2119"},{"id":"25","login":"vologda2120"},{"id":"26","login":"vologda2121"},{"id":"27","login":"vologda2122"},{"id":"28","login":"vologda2123"},{"id":"29","login":"vologda2124"},{"id":"30","login":"vologda2125"},{"id":"31","login":"ekaterinburg2201"},{"id":"32","login":"ekaterinburg2202"},{"id":"33","login":"ekaterinburg2203"},{"id":"34","login":"ekaterinburg2204"},{"id":"35","login":"ekaterinburg2205"},{"id":"36","login":"ekaterinburg2206"},{"id":"37","login":"ekaterinburg2207"},{"id":"38","login":"ekaterinburg2208"},{"id":"39","login":"ekaterinburg2209"},{"id":"40","login":"ekaterinburg2210"},{"id":"41","login":"ekaterinburg2211"},{"id":"42","login":"ekaterinburg2212"},{"id":"43","login":"ekaterinburg2213"},{"id":"44","login":"ekaterinburg2214"},{"id":"45","login":"ekaterinburg2215"},{"id":"46","login":"ekaterinburg2216"},{"id":"47","login":"ekaterinburg2217"},{"id":"48","login":"ekaterinburg2218"},{"id":"49","login":"ekaterinburg2219"},{"id":"50","login":"ekaterinburg2220"},{"id":"51","login":"ekaterinburg2221"},{"id":"52","login":"ekaterinburg2222"},{"id":"53","login":"ekaterinburg2223"},{"id":"54","login":"ekaterinburg2224"},{"id":"55","login":"ekaterinburg2225"},{"id":"56","login":"ekaterinburg2226"},{"id":"57","login":"ekaterinburg2227"},{"id":"58","login":"ekaterinburg2228"},{"id":"59","login":"ekaterinburg2229"},{"id":"60","login":"ekaterinburg2230"},{"id":"61","login":"ekaterinburg2231"},{"id":"62","login":"ekaterinburg2232"},{"id":"63","login":"ekaterinburg2233"},{"id":"64","login":"ekaterinburg2234"},{"id":"65","login":"ekaterinburg2235"},{"id":"66","login":"ekaterinburg2236"},{"id":"67","login":"ekaterinburg2237"},{"id":"68","login":"ekaterinburg2238"},{"id":"69","login":"ekaterinburg2239"},{"id":"70","login":"ekaterinburg2240"},{"id":"71","login":"izhevsk2301"},{"id":"72","login":"izhevsk2302"},{"id":"73","login":"izhevsk2303"},{"id":"74","login":"izhevsk2304"},{"id":"75","login":"izhevsk2305"},{"id":"76","login":"izhevsk2306"},{"id":"77","login":"izhevsk2307"},{"id":"78","login":"izhevsk2308"},{"id":"79","login":"izhevsk2309"},{"id":"80","login":"izhevsk2310"},{"id":"81","login":"izhevsk2311"},{"id":"82","login":"izhevsk2312"},{"id":"83","login":"izhevsk2313"},{"id":"84","login":"izhevsk2314"},{"id":"85","login":"izhevsk2315"},{"id":"86","login":"izhevsk2316"},{"id":"87","login":"izhevsk2317"},{"id":"88","login":"izhevsk2318"},{"id":"89","login":"izhevsk2319"},{"id":"90","login":"izhevsk2320"},{"id":"91","login":"izhevsk2321"},{"id":"92","login":"izhevsk2322"},{"id":"93","login":"izhevsk2323"},{"id":"94","login":"izhevsk2324"},{"id":"95","login":"izhevsk2325"},{"id":"96","login":"izhevsk2326"},{"id":"97","login":"izhevsk2327"},{"id":"98","login":"izhevsk2328"},{"id":"99","login":"izhevsk2329"},{"id":"100","login":"izhevsk2330"},{"id":"101","login":"kemerovo2401"},{"id":"102","login":"kemerovo2402"},{"id":"103","login":"kemerovo2403"},{"id":"104","login":"kemerovo2404"},{"id":"105","login":"kemerovo2405"},{"id":"106","login":"kemerovo2406"},{"id":"107","login":"kemerovo2407"},{"id":"108","login":"kemerovo2408"},{"id":"109","login":"kemerovo2409"},{"id":"110","login":"kemerovo2410"},{"id":"111","login":"kemerovo2411"},{"id":"112","login":"kemerovo2412"},{"id":"113","login":"kemerovo2413"},{"id":"114","login":"kirov2501"},{"id":"115","login":"kirov2502"},{"id":"116","login":"kirov2503"},{"id":"117","login":"kirov2504"},{"id":"118","login":"kirov2505"},{"id":"119","login":"kirov2506"},{"id":"120","login":"kirov2507"},{"id":"121","login":"kirov2508"},{"id":"122","login":"kirov2509"},{"id":"123","login":"kirov2510"},{"id":"124","login":"kirov2511"},{"id":"125","login":"kirov2512"},{"id":"126","login":"kirov2513"},{"id":"127","login":"kirov2514"},{"id":"128","login":"kirov2515"},{"id":"129","login":"kirov2516"},{"id":"130","login":"kirov2517"},{"id":"131","login":"kirov2518"},{"id":"132","login":"kirov2519"},{"id":"133","login":"kirov2520"},{"id":"134","login":"kostroma2601"},{"id":"135","login":"kostroma2602"},{"id":"136","login":"kostroma2603"},{"id":"137","login":"kostroma2604"},{"id":"138","login":"kostroma2605"},{"id":"139","login":"kostroma2606"},{"id":"140","login":"kostroma2607"},{"id":"141","login":"kostroma2608"},{"id":"142","login":"kostroma2609"},{"id":"143","login":"kostroma2610"},{"id":"144","login":"kostroma2611"},{"id":"145","login":"kostroma2612"},{"id":"146","login":"kostroma2613"},{"id":"147","login":"kostroma2614"},{"id":"148","login":"kostroma2615"},{"id":"149","login":"kostroma2616"},{"id":"150","login":"kostroma2617"},{"id":"151","login":"kostroma2618"},{"id":"152","login":"kostroma2619"},{"id":"153","login":"kostroma2620"},{"id":"154","login":"kurgan2701"},{"id":"155","login":"kurgan2702"},{"id":"156","login":"kurgan2703"},{"id":"157","login":"kurgan2704"},{"id":"158","login":"kurgan2705"},{"id":"159","login":"kurgan2706"},{"id":"160","login":"kurgan2707"},{"id":"161","login":"kurgan2708"},{"id":"162","login":"kurgan2709"},{"id":"163","login":"kurgan2710"},{"id":"164","login":"kurgan2711"},{"id":"165","login":"kurgan2712"},{"id":"166","login":"kurgan2713"},{"id":"167","login":"kurgan2714"},{"id":"168","login":"kurgan2715"},{"id":"169","login":"kurgan2716"},{"id":"170","login":"kurgan2717"},{"id":"171","login":"kurgan2718"},{"id":"172","login":"kurgan2719"},{"id":"173","login":"kurgan2720"},{"id":"174","login":"kurgan2721"},{"id":"175","login":"kurgan2722"},{"id":"176","login":"kurgan2723"},{"id":"177","login":"kurgan2724"},{"id":"178","login":"kurgan2725"},{"id":"179","login":"lipeck2801"},{"id":"180","login":"lipeck2802"},{"id":"181","login":"lipeck2803"},{"id":"182","login":"lipeck2804"},{"id":"183","login":"lipeck2805"},{"id":"184","login":"lipeck2806"},{"id":"185","login":"lipeck2807"},{"id":"186","login":"lipeck2808"},{"id":"187","login":"lipeck2809"},{"id":"188","login":"lipeck2810"},{"id":"189","login":"lipeck2811"},{"id":"190","login":"lipeck2812"},{"id":"191","login":"lipeck2813"},{"id":"192","login":"lipeck2814"},{"id":"193","login":"lipeck2815"},{"id":"194","login":"lipeck2816"},{"id":"195","login":"lipeck2817"},{"id":"196","login":"lipeck2818"},{"id":"197","login":"lipeck2819"},{"id":"198","login":"lipeck2820"},{"id":"199","login":"lipeck2821"},{"id":"200","login":"lipeck2822"},{"id":"201","login":"lipeck2823"},{"id":"202","login":"lipeck2824"},{"id":"203","login":"lipeck2825"},{"id":"204","login":"lipeck2826"},{"id":"205","login":"lipeck2827"},{"id":"206","login":"lipeck2828"},{"id":"207","login":"lipeck2829"},{"id":"208","login":"lipeck2830"},{"id":"209","login":"magnitogorsk2901"},{"id":"210","login":"magnitogorsk2902"},{"id":"211","login":"magnitogorsk2903"},{"id":"212","login":"magnitogorsk2904"},{"id":"213","login":"magnitogorsk2905"},{"id":"214","login":"magnitogorsk2906"},{"id":"215","login":"magnitogorsk2907"},{"id":"216","login":"magnitogorsk2908"},{"id":"217","login":"magnitogorsk2909"},{"id":"218","login":"magnitogorsk2910"},{"id":"219","login":"magnitogorsk2911"},{"id":"220","login":"magnitogorsk2912"},{"id":"221","login":"magnitogorsk2913"},{"id":"222","login":"magnitogorsk2914"},{"id":"223","login":"magnitogorsk2915"},{"id":"224","login":"magnitogorsk2916"},{"id":"225","login":"magnitogorsk2917"},{"id":"226","login":"magnitogorsk2918"},{"id":"227","login":"magnitogorsk2919"},{"id":"228","login":"magnitogorsk2920"},{"id":"229","login":"magnitogorsk2921"},{"id":"230","login":"magnitogorsk2922"},{"id":"231","login":"magnitogorsk2923"},{"id":"232","login":"magnitogorsk2924"},{"id":"233","login":"magnitogorsk2925"},{"id":"234","login":"nchelni3001"},{"id":"235","login":"nchelni3002"},{"id":"236","login":"nchelni3003"},{"id":"237","login":"nchelni3004"},{"id":"238","login":"nchelni3005"},{"id":"239","login":"nchelni3006"},{"id":"240","login":"nchelni3007"},{"id":"241","login":"nchelni3008"},{"id":"242","login":"nchelni3009"},{"id":"243","login":"nchelni3010"},{"id":"244","login":"nchelni3011"},{"id":"245","login":"nchelni3012"},{"id":"246","login":"nchelni3013"},{"id":"247","login":"nchelni3014"},{"id":"248","login":"nchelni3015"},{"id":"249","login":"nchelni3016"},{"id":"250","login":"nchelni3017"},{"id":"251","login":"nchelni3018"},{"id":"252","login":"nchelni3019"},{"id":"253","login":"nchelni3020"},{"id":"254","login":"nchelni3021"},{"id":"255","login":"nchelni3022"},{"id":"256","login":"nchelni3023"},{"id":"257","login":"nchelni3024"},{"id":"258","login":"nchelni3025"},{"id":"259","login":"siktivkar3101"},{"id":"260","login":"siktivkar3102"},{"id":"261","login":"siktivkar3103"},{"id":"262","login":"siktivkar3104"},{"id":"263","login":"siktivkar3105"},{"id":"264","login":"siktivkar3106"},{"id":"265","login":"siktivkar3107"},{"id":"266","login":"siktivkar3108"},{"id":"267","login":"siktivkar3109"},{"id":"268","login":"siktivkar3110"},{"id":"269","login":"siktivkar3111"},{"id":"270","login":"siktivkar3112"},{"id":"271","login":"siktivkar3113"},{"id":"272","login":"siktivkar3114"},{"id":"273","login":"siktivkar3115"},{"id":"274","login":"tver3201"},{"id":"275","login":"tver3202"},{"id":"276","login":"tver3203"},{"id":"277","login":"tver3204"},{"id":"278","login":"tver3205"},{"id":"279","login":"tver3206"},{"id":"280","login":"tver3207"},{"id":"281","login":"tver3208"},{"id":"282","login":"tver3209"},{"id":"283","login":"tver3210"},{"id":"284","login":"tver3211"},{"id":"285","login":"tver3212"},{"id":"286","login":"tver3213"},{"id":"287","login":"tver3214"},{"id":"288","login":"tver3215"},{"id":"289","login":"tver3216"},{"id":"290","login":"tver3217"},{"id":"291","login":"tver3218"},{"id":"292","login":"tver3219"},{"id":"293","login":"tver3220"},{"id":"294","login":"ulan_ude3301"},{"id":"295","login":"ulan_ude3302"},{"id":"296","login":"ulan_ude3303"},{"id":"297","login":"ulan_ude3304"},{"id":"298","login":"ulan_ude3305"},{"id":"299","login":"ulan_ude3306"},{"id":"300","login":"ulan_ude3307"},{"id":"301","login":"ulan_ude3308"},{"id":"302","login":"ulan_ude3309"},{"id":"303","login":"ulan_ude3310"},{"id":"304","login":"ulan_ude3311"},{"id":"305","login":"ulan_ude3312"},{"id":"306","login":"ulan_ude3313"},{"id":"307","login":"ufa3401"},{"id":"308","login":"ufa3402"},{"id":"309","login":"ufa3403"},{"id":"310","login":"ufa3404"},{"id":"311","login":"ufa3405"},{"id":"312","login":"ufa3406"},{"id":"313","login":"ufa3407"},{"id":"314","login":"ufa3408"},{"id":"315","login":"ufa3409"},{"id":"316","login":"ufa3410"},{"id":"317","login":"ufa3411"},{"id":"318","login":"ufa3412"},{"id":"319","login":"ufa3413"},{"id":"320","login":"ufa3414"},{"id":"321","login":"ufa3415"},{"id":"322","login":"ufa3416"},{"id":"323","login":"ufa3417"},{"id":"324","login":"ufa3418"},{"id":"325","login":"ufa3419"},{"id":"326","login":"ufa3420"},{"id":"327","login":"ufa3421"},{"id":"328","login":"ufa3422"},{"id":"329","login":"ufa3423"},{"id":"330","login":"ufa3424"},{"id":"331","login":"ufa3425"},{"id":"332","login":"ufa3426"},{"id":"333","login":"ufa3427"},{"id":"334","login":"ufa3428"},{"id":"335","login":"ufa3429"},{"id":"336","login":"ufa3430"},{"id":"337","login":"cheboks3501"},{"id":"338","login":"cheboks3502"},{"id":"339","login":"cheboks3503"},{"id":"340","login":"cheboks3504"},{"id":"341","login":"cheboks3505"},{"id":"342","login":"cheboks3506"},{"id":"343","login":"cheboks3507"},{"id":"344","login":"cheboks3508"},{"id":"345","login":"cheboks3509"},{"id":"346","login":"cheboks3510"},{"id":"347","login":"cheboks3511"},{"id":"348","login":"cheboks3512"},{"id":"349","login":"cheboks3513"},{"id":"350","login":"cheboks3514"},{"id":"351","login":"cheboks3515"},{"id":"352","login":"cheboks3516"},{"id":"353","login":"cheboks3517"},{"id":"354","login":"cheboks3518"},{"id":"355","login":"cheboks3519"},{"id":"356","login":"cheboks3520"},{"id":"357","login":"cheboks3521"},{"id":"358","login":"cheboks3522"},{"id":"359","login":"cheboks3523"},{"id":"360","login":"cheboks3524"},{"id":"361","login":"cheboks3525"},{"id":"362","login":"cherepovec3601"},{"id":"363","login":"cherepovec3602"},{"id":"364","login":"cherepovec3603"},{"id":"365","login":"cherepovec3604"},{"id":"366","login":"cherepovec3605"},{"id":"367","login":"cherepovec3606"},{"id":"368","login":"cherepovec3607"},{"id":"369","login":"cherepovec3608"},{"id":"370","login":"cherepovec3609"},{"id":"371","login":"cherepovec3610"},{"id":"372","login":"cherepovec3611"},{"id":"373","login":"cherepovec3612"},{"id":"374","login":"cherepovec3613"},{"id":"375","login":"cherepovec3614"},{"id":"376","login":"cherepovec3615"},{"id":"377","login":"cherepovec3616"},{"id":"378","login":"cherepovec3617"},{"id":"379","login":"cherepovec3618"},{"id":"380","login":"cherepovec3619"},{"id":"381","login":"cherepovec3620"},{"id":"382","login":"cherepovec3621"},{"id":"383","login":"cherepovec3622"},{"id":"384","login":"cherepovec3623"},{"id":"385","login":"cherepovec3624"},{"id":"386","login":"cherepovec3625"},{"id":"387","login":"chita3701"},{"id":"388","login":"chita3702"},{"id":"389","login":"chita3703"},{"id":"390","login":"chita3704"},{"id":"391","login":"chita3705"},{"id":"392","login":"chita3706"},{"id":"393","login":"chita3707"},{"id":"394","login":"chita3708"},{"id":"395","login":"chita3709"},{"id":"396","login":"chita3710"},{"id":"397","login":"chita3711"},{"id":"398","login":"chita3712"},{"id":"399","login":"chita3713"},{"id":"400","login":"chita3714"},{"id":"401","login":"chita3715"},{"id":"402","login":"yaroslavl3801"},{"id":"403","login":"yaroslavl3802"},{"id":"404","login":"yaroslavl3803"},{"id":"405","login":"yaroslavl3804"},{"id":"406","login":"yaroslavl3805"},{"id":"407","login":"yaroslavl3806"},{"id":"408","login":"yaroslavl3807"},{"id":"409","login":"yaroslavl3808"},{"id":"410","login":"yaroslavl3809"},{"id":"411","login":"yaroslavl3810"},{"id":"412","login":"yaroslavl3811"},{"id":"413","login":"yaroslavl3812"},{"id":"414","login":"yaroslavl3813"},{"id":"415","login":"yaroslavl3814"},{"id":"416","login":"yaroslavl3815"},{"id":"417","login":"yaroslavl3816"},{"id":"418","login":"yaroslavl3817"},{"id":"419","login":"yaroslavl3818"},{"id":"420","login":"yaroslavl3819"},{"id":"421","login":"yaroslavl3820"},{"id":"422","login":"yaroslavl3821"},{"id":"423","login":"yaroslavl3822"},{"id":"424","login":"yaroslavl3823"},{"id":"425","login":"yaroslavl3824"},{"id":"426","login":"yaroslavl3825"},{"id":"427","login":"yaroslavl3826"},{"id":"428","login":"yaroslavl3827"},{"id":"429","login":"yaroslavl3828"},{"id":"430","login":"yaroslavl3829"},{"id":"431","login":"yaroslavl3830"},{"id":"432","login":"yaroslavl3831"},{"id":"433","login":"yaroslavl3832"},{"id":"434","login":"yaroslavl3833"},{"id":"435","login":"yaroslavl3834"},{"id":"436","login":"yaroslavl3835"},{"id":"437","login":"yaroslavl3836"},{"id":"438","login":"yaroslavl3837"},{"id":"439","login":"yaroslavl3838"},{"id":"440","login":"yaroslavl3839"},{"id":"441","login":"yaroslavl3840"},{"id":"442","login":"yaroslavl3841"},{"id":"443","login":"yaroslavl3842"},{"id":"444","login":"yaroslavl3843"},{"id":"445","login":"yaroslavl3844"},{"id":"446","login":"yaroslavl3845"},{"id":"447","login":"ribinsk3901"},{"id":"448","login":"ribinsk3902"},{"id":"449","login":"ribinsk3903"},{"id":"450","login":"ribinsk3904"},{"id":"451","login":"ribinsk3905"},{"id":"452","login":"ribinsk3906"},{"id":"453","login":"ribinsk3907"},{"id":"454","login":"ribinsk3908"},{"id":"455","login":"ribinsk3909"},{"id":"456","login":"ribinsk3910"},{"id":"457","login":"ribinsk3911"},{"id":"458","login":"ribinsk3912"},{"id":"459","login":"ribinsk3913"},{"id":"460","login":"ribinsk3914"},{"id":"461","login":"ribinsk3915"},{"id":"462","login":"ribinsk3916"},{"id":"463","login":"ribinsk3917"},{"id":"464","login":"ribinsk3918"},{"id":"465","login":"ribinsk3919"},{"id":"466","login":"ribinsk3920"},{"id":"467","login":"admin101"}];

    var id = 0;

    for (var i = dataInterviewers.length - 1; i >= 0; i--) {
        if(dataInterviewers[i]['login'] == login){
            id = dataInterviewers[i]['id'];
        }
    };

    return id;
}

function genereateCode () {
    return Math.floor(Math.random() * (9999 - 1000)) + 1000;
}

function checkNameAndLastName(str) {
    var arr = str.split(' ');
    if (arr.length>1 && arr.length <3) {
        return true;
    } else {
        return false;
    }
}